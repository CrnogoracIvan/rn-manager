import React from 'react';
import { Actions, Stack, Scene, Router } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

const RouterComponent = () => {
    return (
        <Router >
            <Scene key = 'root'>
              
                <Scene key="login" 
                    component={LoginForm}  
                    title='Please login'
                    initial                     
                                        
                />
                <Scene 
                    onRight={() => Actions.employeeCreate()}
                    rightTitle=" + "
                    rightButtonTextStyle={{fontSize: 30, color: 'black'}}
                    key="employeeList" 
                    component={EmployeeList} 
                    title="Employees"
                />
                <Scene 
                    key="employeeCreate"
                    component={EmployeeCreate}
                    title="Create Employee"
                />

                <Scene
                    key="employeeEdit"
                    component={EmployeeEdit}
                    title="Edit employee"
                />
            </Scene>
        </Router>
    );
};

export default RouterComponent;